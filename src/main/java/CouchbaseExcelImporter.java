import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

/**
 * Created by daniel on 4/24/15.
 */

public class CouchbaseExcelImporter {
    public static void main(String[] args) {

        System.out.println("\n\n");
        System.out.println("############################################");
        System.out.println("#        COUCHBASE EXCEL IMPORTER          #");
        System.out.println("############################################\n\n");

        CouchbaseExcelImporter importer = new CouchbaseExcelImporter();

        try {

            FileInputStream file = new FileInputStream(new File("/home/daniel/CMDB_XLSX.xlsx"));
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            Sheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();


            for (Row cells : sheet) {
                for (Cell cell : cells) {
                    switch (cell.getCellType())
                    {
                        case Cell.CELL_TYPE_NUMERIC:
                            System.out.print(cell.getNumericCellValue() + "\t");
                            break;
                        case Cell.CELL_TYPE_STRING:
                            System.out.print(cell.getStringCellValue() + "\t");
                            break;
                    }
                }
                System.out.println("");
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

    }
}
